﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectNetCore_Study.Data.Models
{
    public class ShopCart
    {
        private readonly AppDbContent appDBContent;

        public ShopCart(AppDbContent appDBContent)
        {
            this.appDBContent = appDBContent;
        }

        public string ShopId { get; set; }

        public List<ShopItems> listShopItems { get; set; }

        public static ShopCart GetCart(IServiceProvider services)
        {
            ISession session = services.GetRequiredService<IHttpContextAccessor>()?.HttpContext.Session;
            var context = services.GetService<AppDbContent>();
            string shopCartId = session.GetString("CarId") ?? Guid.NewGuid().ToString();

            session.SetString("CarId", shopCartId);

            return new ShopCart(context) { ShopId = shopCartId };

        }

        public void AddToCart(Car car)
        {
            appDBContent.ShopItems.Add(new ShopItems
            {
                ShopId = ShopId,
                car = car,
                price = car.price
            });

            appDBContent.SaveChanges();
        }

        public List<ShopItems> getShopItems()
        {
            return appDBContent.ShopItems.Where(c => c.ShopId == ShopId).Include(s => s.car).ToList();
        }
    }
}
