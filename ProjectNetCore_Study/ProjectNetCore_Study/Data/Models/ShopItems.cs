﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectNetCore_Study.Data.Models
{
    public class ShopItems
    {
        public int id { get; set; }
        public Car car { get; set; }
        public int price { get; set; }
        public string ShopId { get; set; }
    }
}
