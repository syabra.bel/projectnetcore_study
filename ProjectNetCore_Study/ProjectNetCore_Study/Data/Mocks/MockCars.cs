﻿using ProjectNetCore_Study.Data.Interfaces;
using ProjectNetCore_Study.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectNetCore_Study.Data.Mocks
{
    public class MockCars : IAllCars
    {
        private readonly ICarsCategory _categoryCars = new MockCategory();
        public IEnumerable<Car> Cars { get
            { return new List<Car>
                {
                    new Car {name = "Tesla",
                             shortDesc = "Быстрый автомобиль",
                             longDesc = "Экологичный",
                             img = "/img/tesla.jpg",
                             price = 45000,
                             isFavourite = true,
                             available = true,
                             Category = _categoryCars.AllCategories.First()
                    },
                    new Car {name = "Ford Fiesta",
                             shortDesc = "Тихий и спокойный",
                             longDesc = "Удобный бюджетный автомобиль",
                             img = "/img/fordfiesta.jpg",
                             price = 11000,
                             isFavourite = false,
                             available = true,
                             Category = _categoryCars.AllCategories.Last()
                    },
                    new Car {name = "BMW M3",
                             shortDesc = "Дерзкий и стильный",
                             longDesc = "Удобный автомобиль для городской жизни",
                             img = "/img/bmwm3.jpg",
                             price = 65000,
                             isFavourite = true,
                             available = true,
                             Category = _categoryCars.AllCategories.Last()
                    }
                };
            }
        }
        public IEnumerable<Car> getFavCars { get; set; }

        public Car getObjectCar(int carId)
        {
            throw new NotImplementedException();
        }
    }
}
