﻿using ProjectNetCore_Study.Data.Interfaces;
using ProjectNetCore_Study.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectNetCore_Study.Data.Mocks
{
    public class MockCategory : ICarsCategory
    {
        IEnumerable<Category> ICarsCategory.AllCategories {
            get {
                return new List<Category>
                {
                    new Category{ categoryName = "Электромобили", desc = " автомобиль, приводимый в движение электродвигателем с питанием от независимого источника электроэнергии"},
                    new Category{ categoryName = "Классические автомобили", desc = "Автомобили с двигателем внутреннего сгорания"}
                };
            }
        }
    }
}
