﻿using Microsoft.EntityFrameworkCore;
using ProjectNetCore_Study.Data.Interfaces;
using ProjectNetCore_Study.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectNetCore_Study.Data.Repository
{
    public class CarRepository : IAllCars
    {
        private readonly AppDbContent appDBContent;

        public CarRepository(AppDbContent appDBContent)
        {
            this.appDBContent = appDBContent;
        }
        

        public IEnumerable<Car> Cars => appDBContent.Car.Include(c => c.Category);

        public IEnumerable<Car> getFavCars => appDBContent.Car.Where(p => p.isFavourite).Include(c => c.Category);

        public Car getObjectCar(int carId) => appDBContent.Car.FirstOrDefault(p => p.id == carId);
    }
}
