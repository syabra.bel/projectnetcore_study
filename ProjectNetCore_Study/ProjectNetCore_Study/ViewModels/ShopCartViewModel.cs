﻿using ProjectNetCore_Study.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectNetCore_Study.ViewModels
{
    public class ShopCartViewModel
    {
        public ShopCart shopCart { get; set; }
    }
}
