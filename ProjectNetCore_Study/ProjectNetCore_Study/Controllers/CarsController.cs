﻿using Microsoft.AspNetCore.Mvc;
using ProjectNetCore_Study.Data.Interfaces;
using ProjectNetCore_Study.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectNetCore_Study.Controllers
{
    public class CarsController : Controller
    {
        private readonly IAllCars _allCars;
        private readonly ICarsCategory _allCategories;

        public CarsController(IAllCars iAllCars, ICarsCategory iCarsCat)
        {
            _allCars = iAllCars;
            _allCategories = iCarsCat;
        }

        //<summary>
        //Return view cars
        //</summary>
        public ViewResult ListAllCars()
        {
            CarsListViewModel obj = new CarsListViewModel();
            obj.AllCars = _allCars.Cars;
            obj.currCategory = "Автомобили";

            return View(obj);
        }

    }
}
