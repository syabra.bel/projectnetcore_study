﻿using Microsoft.AspNetCore.Mvc;
using ProjectNetCore_Study.Data.Interfaces;
using ProjectNetCore_Study.Data.Models;
using ProjectNetCore_Study.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectNetCore_Study.Controllers
{
    public class HomeController : Controller
    {
        private IAllCars _carRep;

        public HomeController(IAllCars carRep)
        {
            _carRep = carRep;
        }

        public ViewResult Index()
        {
            var homeCars = new HomeViewModel
            {
                FavCars = _carRep.getFavCars
            };

            return View(homeCars);
        }
    }
}
